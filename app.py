#!/usr/bin/env python3
from __future__ import unicode_literals
import flask
from flask import Flask, render_template, request, flash
import sys
from configparser import ConfigParser

import youtube_dl

import glob

import subprocess

HOST = 'http://192.168.1.24:5000/'
VERSION = {
        'youtube_dl':youtube_dl.version.__version__,
        'python':sys.version
        }

config = ConfigParser()
config.read('yerp.conf')

# begin ytdl block
class MyLogger(object):
    def debug(self, msg):
        pass

    def warning(self, msg):
        pass

    def error(self, msg):
        print(msg)


def my_hook(d):
    if d['status'] == 'finished':
        print('Done downloading, now converting ...')
ydl_opts_audio = {
    'format': 'bestaudio/best',
    'outtmpl': './static/%(title)s.%(ext)s',
    'restrictfilenames': True,
    'postprocessors': [{
        'key': 'FFmpegExtractAudio',
        'preferredcodec': 'mp3',
        'preferredquality': '192',
    }],
    'logger': MyLogger(),
    'progress_hooks': [my_hook],
}
ydl_opts_video = {
    'format': '22',
    'outtmpl': './static/%(title)s.%(ext)s',
    'restrictfilenames': True,
    'logger': MyLogger(),
    'progress_hooks': [my_hook],
}
# end ytdl block
app = Flask(__name__)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=int('5000'))
#
# navigation reachable endpoints
#

@app.route('/')
def goat():
    return render_template('index.html',versions=VERSION)

# audio stuff

@app.route('/audio/rip')
def rip_audio():
    return render_template('audio.html')

@app.route('/audio/play')
def play_audio():
    audio_files = glob.glob("static/*.mp3")
    audio_details = []
    files = ''
    for song in audio_files:
        file_details = {'path':song, 'name':song.strip('static/').strip('.mp3').replace('_',' ')}
        audio_details.append(file_details)  
    return render_template('audio-play.html',contents=audio_details)

# video stuff

@app.route('/video/rip')
def rip_video():
    return render_template('video.html')

@app.route('/video/play')
def play_video():
        video_files = glob.glob("static/*.mp4")
        video_details = []
        files = ''
        for video in video_files:
            file_details = {'path': video, 'name': video.strip('static/').strip('.mp4').replace('_',' ')}
            video_details.append(file_details)
        return render_template('video-play.html',contents=video_details)

# ripping endpoints

@app.route('/rip/audio', methods=['POST'])
def audio_ripper():
    if request.method == 'POST':
        # needs file-clobber check
        file_clobber = config.get('global','file_clobber')
        print("Clobber Status: %s" % file_clobber)
        # config file is only read at app start, does not dynamically refresh
        vid = str(request.form['video_id'])
        msg = "Process: "+str(vid)
        with youtube_dl.YoutubeDL(ydl_opts_audio) as ydl:
            ydl.download([vid])
        return render_template('index.html')
@app.route('/rip/video', methods=['POST'])
def video_ripper():
        if request.method == 'POST':
                vid = str(request.form['video_id'])
                msg = "Process: "+str(vid)
                with youtube_dl.YoutubeDL(ydl_opts_video) as ydl:
                    ydl.download([vid])
                return render_template('index.html')

@app.route('/admin/update-yt')
def update_ytdl():
    subprocess.check_call([sys.executable, "-m", "pip", "install", "youtube-dl"])
    return render_template('index.html',versions=VERSION)
""" 

THINGS TO DO

- implement a simple class to wrap around youtube-dl
- flesh out storage mechanism of some kind, probably in a single directory
- find some kind of js-based file browser to integrate
  - this is proving to be a serious pain point, need some kind of solution
    to allow for setting of initial volume levels so ears arent blown out
- research way to initiate download instead of playing of files
- figure out how to do better filenames, specifically dealing with accent makes like '

"""
