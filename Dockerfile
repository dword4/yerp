FROM alpine:3.9
MAINTAINER Drew Hynes <drew.hynes@gmail.com>

# update
RUN apk update

# setup and clone
RUN mkdir -p /yerp
WORKDIR /yerp
RUN apk add git
RUN apk add py-pip
RUN apk add ffmpeg
RUN git clone https://gitlab.com/dword4/yerp .
RUN pip install -r requirements.txt

# set timezone
RUN apk add tzdata
ENV TZ=America/New_York

# make it happen captain!
ENV FLASK_APP=app.py
EXPOSE 5000
CMD ["flask", "run", "--host=0.0.0.0"]

