Here is what I used

click==7.1.2
Flask==1.1.2
itsdangerous==1.1.0
Jinja2==2.11.2
MarkupSafe==1.1.1
Werkzeug==1.0.1
youtube-dl==2020.3.24

And its built on the latest python verson

Python 3.8.2

And confirmed functional with Alpine 3.9 on Docker version 19.03.8, build afacb8b